import base64
import subprocess
import uuid

import boto3
import requests

from geopy.distance import vincenty
from geopy.geocoders import Nominatim

from chalice import Chalice
from chalice import BadRequestError, NotFoundError


app = Chalice(app_name='api')
app.debug = True

s3 = boto3.client('s3', region_name='us-east-1')
BUCKET = 'chalice-bucket'

app = Chalice(app_name='grail')


@app.route('/bikes', methods=['GET', 'POST',])
def find_bikes():
    """Find City bikes with a radius of an address"""

    body = app.current_request.json_body
    country = body.get('country', 'US')
    travel_distance = body.get('distance', 20)
    raw_address = body.get('address', '156 Schraalenburgh Road, Harrington Park NJ')

    api_url = "http://api.citybik.es/v2/"

    networks_url = "{}networks".format(api_url)
    r = requests.get(networks_url)
    if not r.ok:
        raise NotFoundError("Could not connect to CityBikes API")

    d = r.json()
    networks = [network for network in d['networks'] if network['location']['country'] == country]

    geolocator = Nominatim(scheme='http')# TODO my SSL seems broken

    current_location = geolocator.geocode(raw_address).raw
    me = (current_location['lat'], current_location['lon'])

    bikes = []
    for network in networks:
        location = geolocator.reverse("{}, {}".format(network['location']['latitude'], network['location']['longitude']))
        net_location = (location.raw['lat'], location.raw['lon'])
        dist = vincenty(net_location, me).miles
        if dist <= int(travel_distance):
            network_url = "{}networks/{}".format(api_url, network['id'])
            resp = requests.get(network_url)
            if not resp.ok:
                raise NotFoundError("2nd Could not connect to CityBikes API")
            data = resp.json()
            stations = data['network']['stations']

            for station in stations:
                available_bikes = station['free_bikes']
                if available_bikes:
                    station_location = (station['latitude'], station['longitude'])
                    station_dist = vincenty(station_location, me).miles
                    bikes.append({
                        'bikes': available_bikes,
                        'station': station['name'],
                        'miles_from_you': round(station_dist, 3)
                        }
                    )

    return {
        'bikes': (sorted(bikes, key=lambda k: k['bikes']))
    }


@app.route('/status', methods=['GET', ])
def status():
    """just uptime of the host"""
    return {'status': "{}".format(subprocess.check_output('uptime').decode().replace("\n", ""))}


@app.route('/upload', methods=['POST', ])
def upload():
    """upload a JPG image and store it on S3"""
    body = app.current_request.json_body
    img = base64.b64decode(body['data'])
    if not img:
        raise BadRequestError("No file to upload")

    filename = '{}.{}'.format(uuid.uuid4(), 'jpg')
    s3.put_object(ACL='public-read', Body=img, Bucket=BUCKET, Key=filename)

    return {
         'url': 'https://s3.amazonaws.com/{}/{}'.format(BUCKET, filename)
    }