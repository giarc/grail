Coding Challenge with chalice
=============================
Overall, this was a fun experience as I had never even heard of chalice. I can see myself using something like chalice to prototype an API in the future. Its a very interesting idea to be completely 'serverless'. I spent a little more than 4 hours learning chalice and writing the 3 endpoints and I found working with it mostly intuitive. 

A couple of things I hope they improve: its a little cumbersome even with the local microservice. I think a reload on file changes would be great. Also, once deployed, the error messages when things were wrong were not very accurate, often just reporting that authentication token was missing when in fact the URL was just wrong. Also, I created a python virtualenv, and interestingly with the builtin venv that ships as part of Python3, chalice didn't work. 


```
cwt@blue.local:~$ python3 -m venv Desktop/grail(grail) 
...
cwt@blue.local:py3test$ chalice deploy
Initial creation of lambda function.
Creating role
Creating deployment package.
ERROR: The executable /Users/cwt/Desktop/py3test/.chalice/venv/bin/python3 is not functioning
ERROR: It thinks sys.prefix is '/Library/Frameworks/Python.framework/Versions/3.6' (should be '/Users/cwt/Desktop/py3test/.chalice/venv')
ERROR: virtualenv is not compatible with this system or executable
(grail) cwt@blue.local:py3test$
```


API Status
-----------
I read the chalice docs available online
[on github](https://github.com/awslabs/chalice) and after exploring for a little while decided something that could reflect status is the host's uptime. This is a rather boring interpretation of system status.

To run this endpoint:

```
(chalice) cwt@blue.local:grail$ http https://c7fipzfmvk.execute-api.us-east-1.amazonaws.com/dev/status
```


Transform API data into something fun
--------------------------------------
I chose the [citybikes API](http://api.citybik.es/v2/) as I love to ride bicycles and thought it would be fun to see what information they had available. To build something at least possibly useful, I decided to take an address and geolocate city bike locations within a radius of that address. The endpoint takes  country abbreviation, defaulting to the 'US', a distance meant to represent how far away your willing to walk to get a city bike, defaulting to 20 miles, and an address, defaulting to my home address meant to represent your current location.

I think this was a fun one to write, and while its very slow( no cache or storing of data ), the idea could be expanded by periodically checking the API and storing the various 'free_bikes' data so that this API could first try a local store for data, and only hit the citybikes API if local data were stale or missing. Also, with a local datastore, geolocation looks could be stored which would also increase speed.

To run this endpoint:

```
(chalice) cwt@blue.local:grail$ http https://c7fipzfmvk.execute-api.us-east-1.amazonaws.com/dev/bikes country=US distance=20 address="156 Schraalenburgh Road, Harrington Park NJ"
```



Upload an image to S3
----------------------
I take a base64 encoded a JPG file as part of the json request and store it on S3, returning the S3 publicly available url. This is a little short on creativity, but I had a little trouble figuring out how to make the request using httpie, so turned to curl for testing. 

To run this endpoint, I had to experiment a little with curl as I could not seem to send the base64 encoded JPG file without the echo statements you see below. 

 ```
(chalice) cwt@blue.local:grail$ (echo -n '{"data": "'; base64 Falcon.1.jpg; echo '"}') | curl -H "Content-Type: application/json" -d @- https://c7fipzfmvk.execute-api.us-east-1.amazonaws.com/dev/upload
```